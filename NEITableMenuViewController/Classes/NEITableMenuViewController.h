//
// Created by ghost on 3/13/17.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@class NEITableMenuViewController;

typedef NS_ENUM(NSInteger, NEITableMenuItemType) {
    NEITableMenuItemTypeText,
    NEITableMenuItemTypeSection,
    NEITableMenuItemTypeDisabled
} ;



@interface  NEITableMenuItem : NSObject {}
@property (nonatomic, nullable) NSString *label;
@property (nonatomic, assign) NEITableMenuItemType type;
@property (nonatomic, nullable) NSString *identifier;
@property (nonatomic,assign) NSUInteger  index;
@property (nonatomic,assign) NSInteger  tag;
@end

@protocol NEITableMenuDelegate <NSObject>
- (void)tableMenuView:(nonnull NEITableMenuViewController *)tableMenuView didSelectMenuItem:(nonnull NEITableMenuItem  *) menuItem;
@optional
- (void)tableMenuView:(nonnull NEITableMenuViewController *)tableMenuView performedAction:(nonnull id) sender forEvent:(UIEvent*)event;
- (void)tableMenuView:(nonnull NEITableMenuViewController *)tableMenuView didLoadCell: (nonnull  UITableViewCell*) tableViewCell;
@end

@interface  NEITableViewMenuCell : UITableViewCell  {}
@property (nonatomic,retain) NEITableMenuItem *menuItem;
- (void) configureMenuCell;
@end

@interface NEITableMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,retain) IBOutlet UITableView *tableView;
@property (nonatomic, assign) id <NEITableMenuDelegate> delegate;
@property (nonatomic, retain) UIColor * backgroundColor;
@property (nonatomic, copy, nullable) void (^onDismiss)();

- (instancetype)initWithDelegate:(id <NEITableMenuDelegate>)delegate;

+ (instancetype)tableMenuWithDelegate:(id <NEITableMenuDelegate>)delegate;


- (NEITableMenuViewController*) addMenuItemLabel: (NSString *) name
                                            type: (NEITableMenuItemType) type
                                             tag: (NSInteger) tag
                                      identifier: (NSString *) ident NS_SWIFT_NAME(add(menu:type:tag:identifier:));

- (NEITableMenuViewController*) addMenuItemLabel: (NSString *) name
                                            type: (NEITableMenuItemType) type
                                      identifier: (NSString *) ident NS_SWIFT_NAME(add(menu:type:identifier:));

- (NEITableMenuViewController*) addMenuItemLabel: (NSString *) name
                                             tag: (NSUInteger) tag
                                      identifier: (NSString *) ident  NS_SWIFT_NAME(add(menu:tag:identifier:));

- (NEITableMenuViewController*) addMenuItemLabel: (NSString *) name
                                      identifier: (NSString *) ident NS_SWIFT_NAME(add(menu:identifier:));

- (NEITableMenuViewController*) addMenuItemLabel: (NSString *) name
                                            type: (NEITableMenuItemType) type NS_SWIFT_NAME(add(menu:type:));

- (NEITableMenuViewController*) addMenuItemLabel: (NSString *) name NS_SWIFT_NAME(add(menu:));

- (IBAction) dismissMenu: (id) sender;

- (IBAction) performAction: (id) sender forEvent:(UIEvent*)event;

- (void) dismissMenu: (id) sender then: (void (^ __nullable)(void))then;
@end