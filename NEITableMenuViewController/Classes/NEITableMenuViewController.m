//
// Created by ghost on 3/13/17.
//

#import "NEITableMenuViewController.h"
#import <NEICategorizedData/NEICategorizedData.h>

@implementation  NEITableViewMenuCell {
    NEITableMenuItem * _menuItem;
}

- (NEITableMenuItem *)menuItem {
    return _menuItem;
}

- (void)setMenuItem:(NEITableMenuItem *)menuItem {
    _menuItem = menuItem;
    [self configureMenuCell];
}

- (void) configureMenuCell  {}

@end

@implementation  NEITableMenuItem
@end


@interface  NEITableMenuViewController ()
@property (nonatomic,retain) NEICategorizedData *tableViewData;
@property (nonatomic,assign) NSUInteger  index;
@property (nonatomic, retain) NSMutableDictionary *menuItemHeight;
@end

@implementation NEITableMenuViewController {}

- (instancetype)initWithDelegate:(id <NEITableMenuDelegate>)delegate {
    if (self = [super initWithNibName:nil bundle:nil]) {
        self.delegate = delegate;
    }

    return self;
}

+ (instancetype)tableMenuWithDelegate:(id <NEITableMenuDelegate>)delegate {
    NEITableMenuViewController *controller = [[self alloc] initWithDelegate:delegate];
    return controller;
}


- (void) commonInit {
    self.tableViewData = [NEICategorizedData new];
    self.menuItemHeight = [NSMutableDictionary dictionary];
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.backgroundColor = [UIColor clearColor];
    self.index = 0;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if(!self.tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    }

    self.view.backgroundColor =  self.backgroundColor;
    self.view.opaque = false;
    self.tableView.delegate = self;

    NSArray *dataItems =  self.tableViewData.allObjects;
    for(id obj in dataItems) {
        if([obj isKindOfClass:[NEITableMenuItem class]]) {
            NEITableMenuItem * menuItem = (NEITableMenuItem*) obj;
            NSString *ident = menuItem.identifier;
            if(ident && ! self.menuItemHeight[ident]) {
                UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:ident];
                if(cell) {
                    self.menuItemHeight[ident] = @(cell.bounds.size.height);
                }
            }
        }
    }

    [self.tableView reloadData];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self commonInit];
}

- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }

    return self;
}


#pragma  mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.tableViewData numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableViewData numberOfRowsInSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NEICategorizedRow *row = [self.tableViewData rowAtIndexPath:indexPath];
    NEITableMenuItem *menuItem = (NEITableMenuItem *)row.data;
    NSNumber *height = menuItem.identifier ? self.menuItemHeight[menuItem.identifier] : nil;

    if(height) {
        return [height floatValue];
    }

    return 44;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *defaultCellIdentifier = @"defaultTableMenuCellIdentifier";
    NEICategorizedRow *row = [self.tableViewData rowAtIndexPath:indexPath];
    NEITableMenuItem *menuItem = (NEITableMenuItem *)row.data;
    NSString *cellIdentifier = menuItem.identifier ? menuItem.identifier :defaultCellIdentifier;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

    } else if([cell isKindOfClass:[NEITableViewMenuCell class]]) {
        NEITableViewMenuCell *menuCell = (NEITableViewMenuCell*)cell;
        menuCell.menuItem = menuItem;
    } else {
        cell.textLabel.text = menuItem.label;
    }

    cell.userInteractionEnabled = menuItem.type != NEITableMenuItemTypeDisabled;
    cell.tag = menuItem.tag;
    if([self.delegate conformsToProtocol:@protocol(NEITableMenuDelegate)]) {
        if([self.delegate respondsToSelector:@selector(tableMenuView:didLoadCell:)]) {
            [self.delegate tableMenuView:self didLoadCell:cell];
        }
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NEICategorizedRow *row = [self.tableViewData rowAtIndexPath:indexPath];
    NEITableMenuItem *menuItem = (NEITableMenuItem *)row.data;

    if([self.delegate conformsToProtocol:@protocol(NEITableMenuDelegate)]) {
        if([self.delegate respondsToSelector:@selector(tableMenuView:didSelectMenuItem:)]) {
            [self.delegate tableMenuView:self didSelectMenuItem:menuItem];
        }
    }

    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    NEICategorizedRow *row = [self.tableViewData rowAtIndexPath:indexPath];
    NEITableMenuItem *menuItem = (NEITableMenuItem *)row.data;
    BOOL shouldHighlight =  menuItem.type != NEITableMenuItemTypeDisabled;
    return shouldHighlight;
}


#pragma mark -

- (NEITableMenuViewController*) addMenuItemLabel: (NSString *) name
                                            type: (NEITableMenuItemType) type
                                             tag: (NSInteger) tag
                                      identifier: (NSString *) ident {
    NEITableMenuItem *menuItem = [NEITableMenuItem new];

    menuItem.label = name;
    menuItem.identifier = ident;
    menuItem.index = self.index++;
    menuItem.tag = tag;
    menuItem.type = type;

    [self.tableViewData addRowItem:menuItem type:@"item" section:@"menu"];

    return self;
}

- (NEITableMenuViewController*) addMenuItemLabel: (NSString *) name  tag: (NSUInteger) tag identifier: (NSString *) ident  {
    return [self addMenuItemLabel:name type: NEITableMenuItemTypeText tag: tag identifier: ident];
}

- (NEITableMenuViewController*) addMenuItemLabel: (NSString *) name  identifier: (NSString *) ident  {
    return [self addMenuItemLabel:name type: NEITableMenuItemTypeText tag: -1 identifier: ident];
}

- (NEITableMenuViewController*) addMenuItemLabel: (NSString *) name type: (NEITableMenuItemType) type {
    return [self addMenuItemLabel:name type: type  identifier: nil];
}

- (NEITableMenuViewController*) addMenuItemLabel: (NSString *) name   {
    return [self addMenuItemLabel:name type: NEITableMenuItemTypeText  identifier: nil];
}


- (void)performAction:(id)sender forEvent:(UIEvent *)event{
    if([self.delegate conformsToProtocol:@protocol(NEITableMenuDelegate)]) {
        if([self.delegate respondsToSelector:@selector(tableMenuView:performedAction:forEvent:)]) {
            [self.delegate tableMenuView:self performedAction:sender forEvent:event];
        }
    }
}
- (void) dismissMenu: (id) sender {
  [self dismissMenu:sender then: nil];
}

- (void) dismissMenu: (id) sender then: (void (^ __nullable)(void))then {
    [self dismissViewControllerAnimated:true completion:^{
        if(then) {
            then();
        }

        if(self.onDismiss) {
            self.onDismiss();
        }
    }];
}
@end