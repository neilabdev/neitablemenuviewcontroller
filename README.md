# NEITableMenuViewController

[![CI Status](http://img.shields.io/travis/James Whitfield/NEITableMenuViewController.svg?style=flat)](https://travis-ci.org/James Whitfield/NEITableMenuViewController)
[![Version](https://img.shields.io/cocoapods/v/NEITableMenuViewController.svg?style=flat)](http://cocoapods.org/pods/NEITableMenuViewController)
[![License](https://img.shields.io/cocoapods/l/NEITableMenuViewController.svg?style=flat)](http://cocoapods.org/pods/NEITableMenuViewController)
[![Platform](https://img.shields.io/cocoapods/p/NEITableMenuViewController.svg?style=flat)](http://cocoapods.org/pods/NEITableMenuViewController)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NEITableMenuViewController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "NEITableMenuViewController"
```

## Author

James Whitfield, jwhitfield@neilab.com

## License

NEITableMenuViewController is available under the MIT license. See the LICENSE file for more info.
